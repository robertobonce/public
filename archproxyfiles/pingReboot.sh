#!/bin/bash

HOST=google.com

if ping -c 1 -W 2 $HOST &> /dev/null

then
	echo 1
else
	echo 0
	/usr/bin/reboot
fi
