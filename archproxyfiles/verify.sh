#!/bin/bash

FILES=(
#openVPN files
/etc/openvpn/client/client.conf
/etc/openvpn/client/cred
#squid files
/etc/squid/squid.conf
#nftables
/etc/nftables.conf
# pingreboot
/root/pingReboot.sh
/etc/systemd/system/pingreboot.timer
/etc/systemd/system/pingreboot.service
# rebootme
/etc/systemd/system/rebootme.timer
/etc/systemd/system/rebootme.service
# verify script
/root/verify.sh
#test bad
#/root/notfound
)

UNITS=(
openvpn-client@client.service
squid
pingreboot.timer
rebootme.timer
)

FAIL=false

for i in ${FILES[@]}; do
	[ -f ${i} ] || { echo "${i} not exists"; FAIL=true; }
done

for i in ${UNITS[@]}; do
	systemctl is-active --quiet ${i} || { echo "${i} not running"; FAIL=true; }
done

if $FAIL; then
	exit 1
fi

exit 0
